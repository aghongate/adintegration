1] This is integration project with AppDirect
2] Create and cancel subscription is been implemented
3] Request takes the input in the form of xml
3] Following operations are supported with the syntax:

Create subscription:
https://example.com/create?eventUrl={eventUrl}

Cancel subscription:
https://example.com/cancel?eventUrl={eventUrl}

Also implemented the user related operations:

Read user:
https://example.com/account/v1/users/<userId>

Create user:
https://example.com/account/v1/companies/{companyId}/users


4] To make the program running you need to have database in a system. After creating a database you need to update hibernate.hbm.xml file for <DATABASE_USER>, <DATABASE_USER_PASSWORD>, <LISTNER_PORT>, <DATABASE_NAME> 

5] Need to doenload a server public certificate and put it into the project repository so as to make the HttpClient work for HTTPS channel
6] If ssl is enabled on the server side then URL host will change to "https://localhost:8443", rest of the part will remain same
7] To enable ssl on tomcat we can refer the steps mentioned in following link:
https://tomcat.apache.org/tomcat-8.0-doc/ssl-howto.html

HTTPS is SSL over HTTP where server side public certificated gets authenticated by the client. To get this done, on the client side's trustore there must be the public certificated of the server.

7]Basic authentication can be enabled on the tomcat by modifying the following files
In web.xml of the application we need to add following lines at the end:

.....................
<security-constraint>
		<web-resource-collection>
			<web-resource-name>Wildcard means whole app requires authentication</web-resource-name>
			<url-pattern>/rest/*</url-pattern>
			<http-method>GET</http-method>
			<http-method>POST</http-method>
			<http-method>PUT</http-method>
			<http-method>DELETE</http-method>
		</web-resource-collection>
 
      	<auth-constraint>
        	<role-name>tomcat</role-name>
      	</auth-constraint>
</security-constraint>
	
	<login-config>
		<auth-method>BASIC</auth-method>
	</login-config>
	
	<security-role>
     	<description>Normal operator user</description>
     	<role-name>tomcat</role-name>
   	</security-role>
..................... 

And in the tomcat-users.xml file of the tomcat server, we need to add following lines inside <tomcat-users> tag:
..................
<role rolename="tomcat"/>
<user username="tomcat" password="tomcat" roles="tomcat"/>
..................

Implementation details:
1] Here the object which perfrom a subscription operation implemented as a Service. Service is process which starts when the application server starts and can be availed from any part of the application.
2] Hibernate is used to perfrom operations related to subscription with the database.
3] JaxRS is been used to expose the API as rest service
5] HttpClient as a service is implemented to make a eventUrl request.
6] AccountObjectService is been implemented to make operations on Account.
7] UserObjectService is been implemented to make operations on User.
6] manage-services.xml file is a file where the services is been specified for the intitalization.