package com.event.obj;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Creator
{
	@XmlElement
    private String content;

	@XmlElement
    private String lastName;

	@XmlElementWrapper
    private Address address;

	@XmlElement
    private String email;

	@XmlElement
    private String locale;

	@XmlElement
    private String language;

	@XmlElement
    private String uuid;

	@XmlElement
    private String firstName;

	@XmlElement
    private String openId;

    public String getContent ()
    {
        return content;
    }

    public void setContent (String content)
    {
        this.content = content;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public Address getAddress ()
    {
        return address;
    }

    public void setAddress (Address address)
    {
        this.address = address;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getUuid ()
    {
        return uuid;
    }

    public void setUuid (String uuid)
    {
        this.uuid = uuid;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getOpenId ()
    {
        return openId;
    }

    public void setOpenId (String openId)
    {
        this.openId = openId;
    }

}
