package com.event.obj;

import javax.xml.bind.annotation.XmlElement;

public class Address
{
	@XmlElement
    private String lastName;

	@XmlElement
    private String fullName;

	@XmlElement
    private String firstName;

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getFullName ()
    {
        return fullName;
    }

    public void setFullName (String fullName)
    {
        this.fullName = fullName;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

}
