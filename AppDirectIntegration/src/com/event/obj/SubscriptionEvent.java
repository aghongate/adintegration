package com.event.obj;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="event")
public class SubscriptionEvent
{
	@XmlElementWrapper
	private Payload payload;

	@XmlElement
    private String type;

	@XmlElementWrapper
    private Marketplace marketplace;

	@XmlElementWrapper
    private Creator creator;

    public Payload getPayload ()
    {
        return payload;
    }

    public void setPayload (Payload payload)
    {
        this.payload = payload;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public Marketplace getMarketplace ()
    {
        return marketplace;
    }

    public void setMarketplace (Marketplace marketplace)
    {
        this.marketplace = marketplace;
    }

    public Creator getCreator ()
    {
        return creator;
    }

    public void setCreator (Creator creator)
    {
        this.creator = creator;
    }

}
