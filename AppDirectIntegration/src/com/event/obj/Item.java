package com.event.obj;

import javax.xml.bind.annotation.XmlElement;

public class Item
{
	@XmlElement
    private String unit;

	@XmlElement
    private String quantity;

    public String getUnit ()
    {
        return unit;
    }

    public void setUnit (String unit)
    {
        this.unit = unit;
    }

    public String getQuantity ()
    {
        return quantity;
    }

    public void setQuantity (String quantity)
    {
        this.quantity = quantity;
    }

}
