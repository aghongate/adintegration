package com.event.obj;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.persist.PersistanceObject;

@XmlRootElement(name = "user")
public class User extends PersistanceObject {
	
	String firstName;
	String lastName;
	String email;
	String language;
	Set<Account> accounts = new HashSet<Account>(); 
	
	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	public String getFirstName() {
		return firstName;
	}
	
	@XmlElement
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	
	@XmlElement
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}

	@XmlElement
	public void setEmail(String email) {
		this.email = email;
	}

	public String getLanguage() {
		return language;
	}

	@XmlElement
	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean equals(Object obj){
		if (this == obj){
			return true;
		} else if (obj instanceof User){
			if (((User)obj).id.equals(this.id)){
				return true;
			}
		}
		
		return false;

	}

}
