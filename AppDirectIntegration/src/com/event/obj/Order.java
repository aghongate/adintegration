package com.event.obj;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;


public class Order
{
	@XmlElement
    private String pricingDuration;

	@XmlElementWrapper
    private Item item;

	@XmlElement
    private String editionCode;

    public String getPricingDuration ()
    {
        return pricingDuration;
    }

    public void setPricingDuration (String pricingDuration)
    {
        this.pricingDuration = pricingDuration;
    }

    public Item getItem ()
    {
        return item;
    }

    public void setItem (Item item)
    {
        this.item = item;
    }

    public String getEditionCode ()
    {
        return editionCode;
    }

    public void setEditionCode (String editionCode)
    {
        this.editionCode = editionCode;
    }

}
