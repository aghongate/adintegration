package com.event.obj;

import javax.xml.bind.annotation.XmlElement;

public class Marketplace
{
	@XmlElement
    private String baseUrl;

	@XmlElement
    private String partner;

    public String getBaseUrl ()
    {
        return baseUrl;
    }

    public void setBaseUrl (String baseUrl)
    {
        this.baseUrl = baseUrl;
    }

    public String getPartner ()
    {
        return partner;
    }

    public void setPartner (String partner)
    {
        this.partner = partner;
    }

}
