package com.event.obj;

import javax.xml.bind.annotation.XmlElement;

import com.persist.PersistanceObject;

public class Account extends PersistanceObject{

	String status;

	public String getStatus() {
		return status;
	}

	@XmlElement
	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement
	public String getId() {
		return id;
	}
	
	@XmlElement
	public void setId(String id) {
		this.id = id;
	}
	
}
