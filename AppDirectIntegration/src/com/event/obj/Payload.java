package com.event.obj;

import javax.xml.bind.annotation.XmlElementWrapper;


public class Payload
{
	@XmlElementWrapper
    private Order order;

	@XmlElementWrapper
    private Company company;

	@XmlElementWrapper
	private Account account;
	
    public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Order getOrder ()
    {
        return order;
    }

    public void setOrder (Order order)
    {
        this.order = order;
    }

    public Company getCompany ()
    {
        return company;
    }

    public void setCompany (Company company)
    {
        this.company = company;
    }

}
