package com.service;

import com.event.obj.Account;
import com.exception.ManageException;

public interface AccountObjectService {

	public String createAccount(Account account) throws ManageException;
	public boolean updateAccount(Account account) throws ManageException;
	public Account getAccountById(String accountId) throws ManageException;
	public boolean deleteAccountById(String userId) throws ManageException;
}
