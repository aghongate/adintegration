package com.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.event.obj.Account;
import com.event.obj.Company;
import com.event.obj.SubscriptionEvent;
import com.event.obj.User;
import com.exception.ManageException;
import com.persist.PersistanceManager;
import com.persist.PersistanceObject;
import com.rest.response.UserResponse;
import com.rest.util.Constants;
import com.rest.util.SubscriptionUtil;
import com.service.AbstractManageService;
import com.service.UserObjectService;

public final class UserObjectServiceImpl extends AbstractManageService implements UserObjectService{

	private Properties property = null;
	private static Logger log = Logger.getLogger(UserObjectServiceImpl.class);
	
	@Override
	public void initialize(Properties properties) throws ManageException {
		this.property = properties;		
	}

	@Override
	public Properties getProperties() throws ManageException {
		return this.property;
	}

	@Override
	public void start() throws ManageException {
		
	}

	public String createUser(String companyId, User user) throws ManageException{
		if (registerUserWithCompany(companyId, user)){
			return PersistanceManager.getInstance().createOrUpdate(user);
			
		} else {
			return "";
		}
	}
	
	private boolean registerUserWithCompany(String companyId, User user){
		Company company = (Company)PersistanceManager.getInstance().getObject(Company.class, companyId);
		if (!company.getUser().add(user)){
			return false;
		}
		PersistanceManager.getInstance().createOrUpdate(company);
		return true;
	}
	
	
	@Override
	public boolean updateUser(User user) throws ManageException {
		String hql = "SELECT u.id from User u WHERE u.id = " + user.getId();
		String id = (String)PersistanceManager.getInstance().executeQuery(hql).get(0);
		user.setId(id);
		PersistanceManager.getInstance().createOrUpdate(user);
		return true; 
	}

	@Override
	public User getUserById(String userId) throws ManageException {
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put("userId", userId);
		List users = PersistanceManager.getInstance().execiteCriteria(User.class, criteria);
		if (users.size() > 0){
			return (User)users.get(0);
		} else {
			return null;
		}
	}

	@Override
	public boolean deleteUserById(String userId) throws ManageException {
		Map<String, String> criteria = new HashMap<String, String>();
		criteria.put("userId", userId);
		List listOfUsers = PersistanceManager.getInstance().execiteCriteria(User.class, criteria);
		if (listOfUsers.size() != 0){
			User user = (User) listOfUsers.get(0);
			if (user != null){
				PersistanceManager.getInstance().delete(user);
				return true;
			}
		}
		
		return false;
	}

	public String updateUserForAddAccount(Account account, String userIdentifier){
		User user = (User)PersistanceManager.getInstance().getObject(User.class, userIdentifier);
		user.getAccounts().add(account);
		return PersistanceManager.getInstance().createOrUpdate(user);
	}
}
