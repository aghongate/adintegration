package com.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.event.obj.Account;
import com.event.obj.User;
import com.exception.ManageException;
import com.persist.PersistanceManager;
import com.service.AbstractManageService;
import com.service.AccountObjectService;
import com.service.UserObjectService;

public class AccountObjectServiceImpl extends AbstractManageService implements AccountObjectService{

	@Override
	public void initialize(Properties properties) throws ManageException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Properties getProperties() throws ManageException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void start() throws ManageException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String createAccount(Account account) throws ManageException {
		if (account != null){
			PersistanceManager.getInstance().createOrUpdate(account);
			
		}
		return null;
	}

	@Override
	public boolean updateAccount(Account account) throws ManageException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Account getAccountById(String accountId) throws ManageException {
		Map<String, String> criteria = new HashMap<String, String>();		
		criteria.put("accountId", accountId);
		return (Account)PersistanceManager.getInstance().execiteCriteria(Account.class, criteria);

	}

	@Override
	public boolean deleteAccountById(String accountId) throws ManageException {
		if (accountId != null){
			Account account = getAccountById(accountId);
			if (account == null){
				throw new ManageException("Account does not exists in a system");
			}
			PersistanceManager.getInstance().delete(account);
			return true;
		}
		return false;
	}

	
}
