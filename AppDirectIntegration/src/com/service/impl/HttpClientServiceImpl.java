package com.service.impl;

import java.util.Properties;

import com.client.HttpClient;
import com.client.HttpRequest;
import com.client.HttpResponse;
import com.exception.ManageException;
import com.service.AbstractManageService;

public class HttpClientServiceImpl extends AbstractManageService{

	HttpClient client = new HttpClient();
	Properties property = null;
	HttpClientServiceImpl(HttpClient client){
		this.client = client;
	}
	
	@Override
	public void initialize(Properties properties) throws ManageException {
		// Used to intitalise the properties file
		this.property = properties;
		
	}

	@Override
	public Properties getProperties() throws ManageException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void start() throws ManageException {
		// TODO: used to start any other service or any pre-processing required before consume the service
		
	}
	
	
	public HttpResponse getRequest(String url, String outputType){
		HttpRequest request = new HttpRequest();
		request.setUrl(url)
		.setMethod("GET")
		.setOutputType(outputType);
		
		return client.execute(request);
		
	}
	
	public HttpResponse deleteRequest(String url, String outputType){
		HttpRequest request = new HttpRequest();
		request.setUrl(url)
		.setMethod("DELETE")
		.setOutputType(outputType);
		
		return client.execute(request);
		
	}
	
	public HttpResponse putRequest(String url, String body, String inputType, String outputType){
		HttpRequest request = new HttpRequest();
		request.setUrl(url)
		.setMethod("PUT")
		.setInputType(inputType)
		.setOutputType(outputType);
		
		return client.execute(request);
		
	}
	
	public HttpResponse postRequest(String url, String body, String inputType, String outputType){
		HttpRequest request = new HttpRequest();
		request.setUrl(url)
		.setMethod("POST")
		.setBody(body)
		.setInputType(inputType)
		.setOutputType(outputType);
		
		return client.execute(request);
		
	}
}
