package com.service;

import java.util.Properties;

import com.exception.ManageException;

public interface ManageService {
	public void initialize(Properties properties) throws ManageException;
	public Properties getProperties() throws ManageException;
	public void start() throws ManageException;
	public void destroy() throws ManageException;
}
