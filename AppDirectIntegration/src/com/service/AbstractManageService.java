package com.service;

import com.exception.ManageException;

public abstract class AbstractManageService implements ManageService{

	// May be used in future to release the resources associated with the service when server is terminating 
	public void destroy(){}
	
}
