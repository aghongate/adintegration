package com.service;

import com.event.obj.User;
import com.exception.ManageException;

public interface UserObjectService{

	public String createUser(String companyId, User user) throws ManageException;
	public boolean updateUser(User user) throws ManageException;
	public User getUserById(String userId) throws ManageException;
	public boolean deleteUserById(String userId) throws ManageException;
	
}
