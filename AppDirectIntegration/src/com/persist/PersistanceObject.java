package com.persist;

import java.util.Map;

import javax.xml.bind.annotation.XmlTransient;

public class PersistanceObject {

	protected String id;
	
	@XmlTransient
	public String getId() { 
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	// Extending object may perform operation on itself. For future support 
	protected void insertRecord(PersistanceObject obj){
		PersistanceManager.getInstance().createOrUpdate(obj);
	}
	
	protected void updateRecord(PersistanceObject obj){
		PersistanceManager.getInstance().createOrUpdate(obj);
	}
	
	protected PersistanceObject getRecord(Class objClass, Map<String, String> criterias){
		return (PersistanceObject) PersistanceManager.getInstance().execiteCriteria(objClass, criterias);
		
	}
	
	protected void deleteRecord(Class objClass, Map<String, String> criterias){
		PersistanceObject obj = this.getRecord(objClass, criterias);
		
		if (obj != null){
			PersistanceManager.getInstance().delete(obj);
		}
	}
	
	
}
