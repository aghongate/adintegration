package com.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.client.HttpResponse;
import com.event.obj.Account;
import com.event.obj.SubscriptionEvent;
import com.event.obj.User;
import com.exception.ManageException;
import com.persist.PersistanceManager;
import com.rest.response.AccountResponse;
import com.rest.response.CoreResponse;
import com.rest.util.Constants;
import com.rest.util.SubscriptionUtil;
import com.service.impl.AccountObjectServiceImpl;
import com.service.impl.HttpClientServiceImpl;
import com.service.impl.UserObjectServiceImpl;
import com.service.init.Container;
import org.apache.log4j.Logger;
import org.hibernate.exception.ConstraintViolationException;

@Path("/")
public class ManageSubscription {

	private static Logger log = Logger.getLogger(ManageUser.class); 
	
	@GET
	@Path("create")
	@Produces(MediaType.APPLICATION_XML)
	public Response createSubscription(@QueryParam("eventUrl") String eventUrl){
		try {
			return createAccount(URLDecoder.decode(eventUrl, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error("Error while deleting account", e);
			return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new AccountResponse(null, "Internal error", null, "FALSE"));
		}
	}
	
	private Response createAccount(String eventUrl){
		
		int status = Constants.STATUS_BAD_REQUEST;
		Account account = null;
		String identifier = null;
		try{
			HttpResponse response = ((HttpClientServiceImpl)Container.getService(Constants.HTTP_CLIENT_SERVICE)).getRequest(eventUrl, Constants.XML_TYPE);
			
			if (response != null && response.getStatusCode() != Constants.STATUS_OK || response.getStatusCode() != Constants.STATUS_CREATED){
				return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new AccountResponse(response.getBriefCode(), response.getMessage(), null, "FALSE"));
			
			} else if (SubscriptionUtil.isNotNullOrEmpty(response.getMessage())){
				SubscriptionEvent event = (SubscriptionEvent)SubscriptionUtil.createObject(response.getMessage(), SubscriptionEvent.class);
				account = SubscriptionUtil.makeAccountFromEvent(event);
				identifier = ((AccountObjectServiceImpl)Container.getService(Constants.ACCOUNT_SERVICE)).createAccount(account);
				// update user details
				((UserObjectServiceImpl)Container.getService(Constants.USER_SERVICE)).updateUserForAddAccount(account, event.getCreator().getUuid());
				status = Constants.STATUS_CREATED;
				
			} else {
				return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new CoreResponse("Create Subscription is failed", "FALSE", "Create Subscription is failed"));
			}
						
		} catch (ConstraintViolationException violation){
			log.error("Account already exists is system : " + identifier);
			return SubscriptionUtil.createResponse(Constants.STATUS_CONFLICT, new CoreResponse("Subscription already exists", "FALSE", "Subscription already exists"));
		} catch(ManageException e){
			log.error("Failed delete account operation", e);
			return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new AccountResponse(e.getMessage(), "Create Subscription is failed", null, "FALSE"));
		} 
		
		log.debug("Successfully created account : " + account.getId());
		return SubscriptionUtil.createResponse(status, new AccountResponse("", "", identifier, "TRUE"));
		
	}
	
	@GET
	@Path("/cancel")
	@Produces(MediaType.APPLICATION_XML)
	public Response cancelSubscription(@QueryParam("eventUrl") String eventUrl){
		try {
			return deleteAccount(URLDecoder.decode(eventUrl, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error("Error while deleting account", e);
			return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new CoreResponse("Internal error", "FALSE", "Internal error"));
		}
	}
	
	private Response deleteAccount(String eventUrl){
		
		int status = Constants.STATUS_BAD_REQUEST;
		boolean isValid = false;
		try{
			HttpResponse response = ((HttpClientServiceImpl)Container.getService(Constants.HTTP_CLIENT_SERVICE)).deleteRequest(eventUrl, Constants.XML_TYPE);
			
			if (response != null && response.getStatusCode() != Constants.STATUS_OK || response.getStatusCode() != Constants.STATUS_CREATED){
				return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new AccountResponse(response.getBriefCode(), response.getMessage(), null, "FALSE"));
			
			} else if (SubscriptionUtil.isNotNullOrEmpty(response.getMessage())){
				SubscriptionEvent event = (SubscriptionEvent)SubscriptionUtil.createObject(response.getMessage(), SubscriptionEvent.class);
				
				isValid = ((AccountObjectServiceImpl)Container.getService(Constants.ACCOUNT_SERVICE)).deleteAccountById(SubscriptionUtil.extractAccountIdentifier(event));
		     
				if (!isValid){
					return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new CoreResponse("Invalide input", "FALSE", "Invalide input"));
				} 
				status = Constants.STATUS_NO_CONTENT;
			} else {
				return SubscriptionUtil.createResponse(Constants.STATUS_BAD_REQUEST, new CoreResponse("Cancel Subscription is failed", "FALSE", "Cancel Subscription is failed"));
			}
						
		} catch(ManageException e){
			log.error("Delete accout operation failed: " + e.getMessage());
			return SubscriptionUtil.createResponse(Constants.STATUS_NOT_FOUND, new CoreResponse("Account does not exista in syste", "FALSE", "account does not exista in syste"));
		}
		
		return SubscriptionUtil.createResponse(status, new CoreResponse("", "TRUE", ""));
		
	}

}
