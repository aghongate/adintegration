package com.rest.util;

import java.io.StringReader;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.event.obj.Account;
import com.event.obj.SubscriptionEvent;
import com.exception.ManageException;

public class SubscriptionUtil {

	public static Object createObject(String xml, Class objClass) throws ManageException{
		StringReader reader = new StringReader(xml);
		JAXBContext jaxbContext = null;
		Unmarshaller jaxbUnmarshaller = null;
		Object obj = null;
		try {
			jaxbContext = JAXBContext.newInstance(objClass);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			obj = jaxbUnmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			throw new ManageException("Unable to parse xml", e);
		}
		
		return obj;
	}
	
	public static Response createResponse(int status, Object msg){
		return Response.status(status).entity(msg).build();
	}
	
	public static boolean isNotNullOrEmpty(String value){
		if (value != null || !value.isEmpty()){
			return true;
		} else {
			return false;
		}
	}
	
	public static Account makeAccountFromEvent(SubscriptionEvent event){
		
		Account account = new Account();
		account.setId(event.getPayload().getAccount().getId());
		account.setStatus(event.getPayload().getAccount().getStatus());
		
		return account;
	}
	
	public static String extractAccountIdentifier(SubscriptionEvent event){
		return event.getPayload().getAccount().getId();
	}
	
}
