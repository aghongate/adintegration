package com.rest.util;

public interface Constants {

	public static final String USER_SERVICE = "com.service.impl.UserObjectServiceImpl";
	public static final String HTTP_CLIENT_SERVICE = "com.service.impl.HttpClient";
	public static final String ACCOUNT_SERVICE = "com.service.impl.AccountObjectServiceImpl";
	
	public static final int STATUS_OK = 200;
	public static final int STATUS_NO_CONTENT = 204;
	public static final int STATUS_CREATED = 201;
	public static final int STATUS_FOUND = 302;
	public static final int STATUS_BAD_REQUEST = 400;
	public static final int STATUS_UNAUTHORISED = 401;
	public static final int STATUS_NOT_FOUND = 404;
	public static final int STATUS_INTERNAL_SERVER_ERROR = 500;
	public static final int STATUS_CONFLICT = 409;
	public static final String XML_TYPE= "application/xml";
	
	public final static String DUPLICATE_USER="DUPLICATE_USER";
	public final static String USER_CREATED="USER_CREATED";
	public final static String USER_DELETED="USER_DELETED";
	public final static String USER_NOT_FOUND="USER_NOT_FOUND";
	public final static String MISSING_USER_ID = "MISSING_USER_ID";
	public final static String USER_FOUND = "USER_FOUND";

}
