package com.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;

import com.event.obj.User;
import com.exception.ManageException;
import com.rest.response.UserResponse;
import com.rest.util.Constants;
import com.rest.util.SubscriptionUtil;
import com.service.impl.UserObjectServiceImpl;
import com.service.init.Container;

@Path("/account/v1")
public class ManageUser {

	public static final String CREATE_USER = "CreateUser";
	public static final String UPDATE_USER = "UpdateUser";
	public static final String USER_SERVICE = "com.service.UserObjectServiceImpl";
	public static final String HTTP_CLIENT_SERVICE = "com.service.HttpClient";
	private static Logger log = Logger.getLogger(ManageUser.class); 
	
	@POST
	@Path("/companies/{companyId}/users")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response createUser(User user, @PathParam("companyId") String companyId){
		 try {
			String identifier = ((UserObjectServiceImpl)Container.getService(USER_SERVICE)).createUser(companyId, user);
			if (identifier.isEmpty()){
				return SubscriptionUtil.createResponse(Constants.STATUS_CONFLICT, new UserResponse("FALSE", "User already exists", "User already exists"));
			} else {
				return SubscriptionUtil.createResponse(Constants.STATUS_OK, new UserResponse("TRUE", "", ""));
			}
		} catch (ManageException e) {
			return SubscriptionUtil.createResponse(Constants.STATUS_INTERNAL_SERVER_ERROR, new UserResponse("FALSE", "Internal server error", "Internal server error"));
		}
	}
	
	@PUT
	@Path("/users/{userId}")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response updateUser(User user, @PathParam("userId") String userId){
		//ToDo
		return null;
	}
		
	@GET
	@Path("/users/{userId}")
	@Produces(MediaType.APPLICATION_XML)
	public Response getUser(@PathParam("userId")String userId){
		
		if (userId == null || userId.isEmpty()){
			return SubscriptionUtil.createResponse(Constants.STATUS_UNAUTHORISED, new UserResponse("FALSE", Constants.MISSING_USER_ID, "User id is missing"));
		}
		User user = null;
		String errorMsg = "User with user id [" + userId + "] not found.";
		try{
			user = ((UserObjectServiceImpl)Container.getService(USER_SERVICE)).getUserById(userId);
		} catch(ManageException e){
			log.error("User not found");
			return SubscriptionUtil.createResponse(Constants.STATUS_NOT_FOUND, new UserResponse("false", "USER_NOT_FOUND", errorMsg));
		} catch (Exception e){
			log.error("User not found");
			return SubscriptionUtil.createResponse(Constants.STATUS_OK, new UserResponse("false", "INTERNAL_ERROR", "Internal error"));
		}
		if (user != null){
			log.info("User found");
			return SubscriptionUtil.createResponse(Constants.STATUS_FOUND, user);
		} else{
			log.debug("User not found");
			return SubscriptionUtil.createResponse(Constants.STATUS_NOT_FOUND, new UserResponse("false", "USER_NOT_FOUND", errorMsg));
		}
		

	}
	
}
