package com.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="result")
public class UserResponse extends CoreResponse{
	
	private UserResponse(){
	}
	
	public UserResponse(String success, String errorCode, String message){
		this.success = success;
		this.errorcode = errorCode;
		this.messgae = message;
	}

}
