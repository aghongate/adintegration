package com.rest.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="result")
public class AccountResponse extends CoreResponse {

	String accountIdentifier;
	
	private AccountResponse(){
		
	}
	
	public AccountResponse(String errorcode, String message, String accountIdentifier, String success){
		this.errorcode = errorcode;
		this.messgae = message;
		this.accountIdentifier = accountIdentifier;
		this.success = success;
	}
	
	@XmlElement
	public String getAccountIdentifier() {
		return accountIdentifier;
	}
	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}
		
}
