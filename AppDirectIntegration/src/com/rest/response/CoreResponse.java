package com.rest.response;

import javax.xml.bind.annotation.XmlElement;

public class CoreResponse {

	String errorcode;
	String success;
	String messgae;
	
	protected CoreResponse(){
		
	}
	
	public CoreResponse(String errorcode, String success, String message){
		this.errorcode = errorcode;
		this.success = success;
		this.messgae = message;
	}
	
	@XmlElement
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	
	@XmlElement
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
		
	@XmlElement
	public String getMessgae() {
		return messgae;
	}
	public void setMessgae(String messgae) {
		this.messgae = messgae;
	}

}
