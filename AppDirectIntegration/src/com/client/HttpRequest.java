package com.client;

import java.util.HashMap;
import java.util.Map;

public class HttpRequest {

	private String url = null;
	private Map<String, String> requestParam = new HashMap<String, String>();
	private String body = null;
	private String method = null;
	private Map<String, String> postParams = new HashMap<String, String>();
	private String certificatePath = null; // this is support for ssl, provide the public certificate that needs to import into the trustore of the SSLContext
	private boolean isHttps = true;
	private String inputType = "application/text";
	private String outputType = "application/text";
	
	public String getInputType() {
		return inputType;
	}
	public HttpRequest setInputType(String inputType) {
		this.inputType = inputType;
		return this;
	}
	public String getOutputType() {
		return outputType;
	}
	public HttpRequest setOutputType(String outputType) {
		this.outputType = outputType;
		return this;
	}
	public String getUrl() {
		return url;
	}
	public HttpRequest setUrl(String url) {
		this.url = url;
		return this;
	}
	public Map<String, String> getRequestParam() {
		return requestParam;
	}
	public HttpRequest setRequestParam(Map<String, String> requestParam) {
		this.requestParam = requestParam;
		return this;
	}
	public String getBody() {
		return body;
	}
	public HttpRequest setBody(String body) {
		this.body = body;
		return this;
	}
	public String getMethod() {
		return method;
	}
	public HttpRequest setMethod(String method) {
		this.method = method;
		return this;
	}
	public Map<String, String> getPostParams() {
		return postParams;
	}
	public HttpRequest setPostParams(Map<String, String> postParams) {
		this.postParams = postParams;
		return this;
	}
	public String getCertificatePath() {
		return certificatePath;
	}
	public HttpRequest setCertificatePath(String certificatePath) {
		this.certificatePath = certificatePath;
		return this;
	}
	public boolean isHttps() {
		return isHttps;
	}
	public HttpRequest setHttps(boolean isHttps) {
		this.isHttps = isHttps;
		return this;
	}
	
}
