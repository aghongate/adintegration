package com.client;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.log4j.Logger;

import com.exception.ManageException;
import com.rest.util.SubscriptionUtil;

public class HttpClient {

	private static final String PUBLIC_CERT_PATH="<path of the server certificate>"; // Need to download and create a certificate file in der format(plain text certificate) from the server site and put the path of that file here
	private static final String USER_CERT_ALIAS = "cert_";
	
	private static final String HEADER_ACCEPT = "Accept";
	private static final String HEADER_CONTENT_TYPE = "Content-Type";	
	private static final String TYPE_APP_XML="application/xml";
	private static final String HTTP_POST = "POST";
	private static final String HTTP_PUT = "PUT";
	
	private static final Logger log = Logger.getLogger(HttpClient.class);
	
	public HttpResponse execute(HttpRequest request){
		URL url = null;
		HttpResponse response = null;
		HttpsURLConnection connection = null; // Need to change HttpUrlConnectino id URL is not secure
		// this will not check whether the domain in the certificate and the domain from where the certificate is coming during ssl handshake are same or not
		StringBuffer bf = new StringBuffer();
		HostnameVerifier hv = new HostnameVerifier() {
			@Override
            public boolean verify(String urlHostName, SSLSession session) {
                return true;
            }
        };
        
		try {
			url = new URL(request.getUrl());
			connection.setSSLSocketFactory(getSSLContext(readCertificate(null)).getSocketFactory()); // null is passed so as to use the default path
			connection = (HttpsURLConnection)url.openConnection();
			
			for (Map.Entry<String, String> entry : request.getRequestParam().entrySet()){
				connection.setRequestProperty(entry.getKey(), entry.getValue());
			}

			
    		connection.setRequestMethod(request.getMethod());
    		connection.setDoOutput(true);
    		connection.setDoInput(true);
            connection.setRequestProperty(HEADER_ACCEPT, TYPE_APP_XML);
			
			if (request.getMethod().equals(HTTP_POST) || request.getMethod().equals(HTTP_PUT)){
				connection.setRequestProperty(HEADER_CONTENT_TYPE, TYPE_APP_XML);
				String body = request.getBody();
				if (SubscriptionUtil.isNotNullOrEmpty(body)){
					connection.getOutputStream().write(body.getBytes());
				}
				
			}
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine = null;
				while ((inputLine = br.readLine()) != null){
					bf.append(inputLine);
				}
				
				
			} catch (IOException e){
				response.setMessage("Failed to read response from server");
				response.setBriefCode(e.getMessage());
				response.setStatusCode(200);
			}
		
			if (connection != null && (connection.getResponseCode() == 201) 
					               || (connection.getResponseCode() == 200)
					               || (connection.getResponseCode() == 302)){
				response.setMessage(bf.toString());
				response.setStatusCode(200);
			} else {
				response.setMessage("Operation failed");
				response.setStatusCode(200);
			}
			
		} catch (MalformedURLException e) {
			response.setMessage("URL is not correct");
			response.setBriefCode(e.getMessage());
			
		} catch (Exception e){
			response.setMessage("Operation is failed");
			response.setMessage(e.getMessage());
		}
		
		return response;
		
	}
	

	private String readCertificate(String certificatePath) throws ManageException{
		if (certificatePath == null){
			certificatePath = PUBLIC_CERT_PATH;
		}
		File certFile = new File(certificatePath);
		StringBuffer sb = new StringBuffer();
		try {
		BufferedReader br = new BufferedReader(new FileReader(certFile));
		String line = br.readLine();
		while(line != null){
			sb.append(line);
			line = br.readLine();
		}
		} catch (IOException ioExcep){
			log.error("Unable to read certificate file", ioExcep);
			throw new ManageException("Unable to read certificate file", ioExcep);
		}
		return sb.toString();
		
	}
	
	private static SSLContext getSSLContext(String certStr) throws Exception {
        SSLContext context = null;
        TrustManager[] trustManagers = null;
        try {
            context = SSLContext.getInstance("TLS");
            trustManagers = getTrustManagers(certStr);
            context.init(null, trustManagers, null);
        } catch (Exception exp) {
            log.error("Unable to create SSLContext", exp);
            throw new ManageException("Unable to create SSLContext", exp);
        }
        return context;
    }
	
	private static TrustManager[] getTrustManagers(String certificateStr) throws ManageException {
	       KeyStore keyStore = null;
	       TrustManagerFactory factory = null;
	        try{
	            certificateStr = normaliseCertificate(certificateStr);
	            Certificate cert = createCertificate(certificateStr);
	            keyStore = keyStore.getInstance(KeyStore.getDefaultType());
	            keyStore.load(null);
	            String certAlias = USER_CERT_ALIAS + generateRandomNumber();
	            keyStore.setCertificateEntry(certAlias,cert);
	            factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
	            factory.init(keyStore);
	        }catch (Exception exp){
	            log.error("Trustmanager configuration error",exp);
	            throw new ManageException("Trustmanager configuration error", exp);
	        }
	        return factory.getTrustManagers();
	}

	private static String generateRandomNumber(){
        Random randomGenerator = new Random();
        return Integer.toString(randomGenerator.nextInt(9000));
    }
	
	// To remove extra spaces from the certificate string
	private static String normaliseCertificate(String inpString){
        String fPart = null;
        String mPart = null;
        String lPart = null;
        String outString = null;

        Pattern pattern = Pattern.compile("^-+[^-]+-+");
        Matcher matcher = pattern.matcher(inpString);

        if(matcher.find()){
            fPart = matcher.group();
        }
        String[] splitedParts = inpString.split("^-+[^-]+-+");
        String remainingPart = null;

        String[] parts = null;
        if(splitedParts.length == 2){
            remainingPart = splitedParts[1];
        }

        if(remainingPart != null){
            pattern = Pattern.compile("-+[^-]+-+$");
            matcher = pattern.matcher(remainingPart);
            if(matcher.find()){
                lPart = matcher.group();

                parts = remainingPart.split("-+[^-]+-+$");
                if(parts.length > 0)
                    mPart = parts[0];
            }
        }

        if(fPart!= null && mPart!= null && lPart!= null){
            mPart=mPart.replaceAll(" ", "");
            outString = fPart+"\n"+mPart+"\n"+lPart;
        } else{
            outString = null;
        }
        return outString;
    }

	private static Certificate createCertificate(String certificate) throws ManageException{
        Certificate publicCert = null;
        ByteArrayInputStream inCert = new ByteArrayInputStream(certificate.getBytes());
        try{
        	publicCert = CertificateFactory.getInstance("X.509").generateCertificate(inCert);
        } catch (CertificateException e){
            log.error("Certificate creation error", e);
            throw new ManageException("Certificate creation error", e);
        }

        return publicCert;
    }

}
