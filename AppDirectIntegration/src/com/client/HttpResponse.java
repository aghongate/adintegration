package com.client;

public class HttpResponse {

	int statusCode = 0;
	String errorMessage = null;
	String message = null;
	String briefCode = null;
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBriefCode() {
		return briefCode;
	}
	public void setBriefCode(String briefCode) {
		this.briefCode = briefCode;
	}
	
}
